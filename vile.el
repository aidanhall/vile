;;; vile.el --- Vim Like Editing. -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Aidan Hall

;; Author: Aidan Hall <aidan.hall202@gmail.com>
;; Keywords: emulations
;; Requires: emacs-28.1 compat-29
;; Version: 0.0.1
;; Website: https://gitlab.com/aidanhall/vile

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This package provides `vile-mode', a global minor mode that provides editor
;; behaviour that tries to closely mimic Vim's Normal mode, with the bare
;; minimum of added code.  This means sacrificing perfect compatibility with Vim
;; keys where an existing Emacs function's behaviour differs slightly from it.
;;
;; I created this package because I wasn't happy about how big Evil mode seems
;; to be, when much of the benefit of Vim-like editing could be achieved with
;; just a keymap and a small number of  functions.
;;
;; `vile-mode' is one plain minor mode, currently with no added abstractions
;; like states in Evil.  The equivalent to Vim's insert mode in Vile is
;; `(vile-mode 0)', which is in fact exactly how the i key is implemented.
;; Vile is laser-focussed on supporting editing that feels a bit like Vim, and
;; gets out of your way as much as possible.

;;; Code:

(require 'compat)
(require 'cl-seq)
(require 'cl-macs)

(defvar vile-mode-map
  (make-sparse-keymap))

(defmacro vile-define-composition (name args key &rest forms)
  "Define `NAME' on `ARGS', to run `FORMS', bound to `KEY' in `vile-mode-map'."
  `(progn
     (defun ,name ,args
       ,(concat
         "Executes the following sequence of editing commands:\n"
         (with-temp-buffer
           (cl-prettyprint
            (cl-remove-if (lambda (form) (eq (car form) 'interactive))
                       forms))
           (buffer-string)))
       ,@forms)
     (define-key vile-mode-map ,key ',name)))

;;;; Cursor Motion
(define-key vile-mode-map "1" 'digit-argument)
(define-key vile-mode-map "2" 'digit-argument)
(define-key vile-mode-map "3" 'digit-argument)
(define-key vile-mode-map "4" 'digit-argument)
(define-key vile-mode-map "5" 'digit-argument)
(define-key vile-mode-map "6" 'digit-argument)
(define-key vile-mode-map "7" 'digit-argument)
(define-key vile-mode-map "8" 'digit-argument)
(define-key vile-mode-map "9" 'digit-argument)

(define-key vile-mode-map "j" 'next-line)
(define-key vile-mode-map "k" 'previous-line)
;; TODO: Handle `end-point' past start or end of buffer.
(vile-define-composition
 vile-backward-char () "h"
 (interactive)
 (let ((end-point
        (save-excursion
          (call-interactively #'backward-char)
          (point))))
   (if (= (line-number-at-pos) (line-number-at-pos end-point))
       (goto-char end-point)
     (move-beginning-of-line 1))))
(vile-define-composition
 vile-forward-char () "l"
 (interactive)
 (let ((end-point
        (save-excursion
          (call-interactively #'forward-char)
          (point))))
   (if (= (line-number-at-pos) (line-number-at-pos end-point))
       (goto-char end-point)
     (move-end-of-line 1))))

(define-key vile-mode-map "0" 'move-beginning-of-line)
(define-key vile-mode-map "$" 'move-end-of-line)
(define-key vile-mode-map "^" 'back-to-indentation)
(define-key vile-mode-map "w" 'forward-word)
(define-key vile-mode-map "b" 'backward-word)
(define-key vile-mode-map "B" 'backward-sexp)
(define-key vile-mode-map "W" 'forward-sexp)
(define-key vile-mode-map "G" 'end-of-buffer)
(define-key vile-mode-map "gg" 'beginning-of-buffer)
;; These bindings in fact mean the opposite of what they do in Vim,
;; because Emacs makes "visual" line movement the default.
(define-key vile-mode-map "gj" 'next-logical-line)
(define-key vile-mode-map "gk" 'previous-logical-line)

(define-key vile-mode-map "{" 'backward-paragraph)
(define-key vile-mode-map  "}" 'forward-paragraph)
(define-key vile-mode-map "(" 'backward-sentence)
(define-key vile-mode-map  ")" 'forward-sentence)

(define-key vile-mode-map "[[" 'beginning-of-defun)
(define-key vile-mode-map "]]" 'end-of-defun)

;;;; Editing:
;; In version 0.0.1 we make the simplification of not having <command> <motion>
;; semantics.

(vile-define-composition
 vile-change () (kbd "c")
 (interactive)
 (call-interactively #'kill-region)
 (vile-mode 0))

(vile-define-composition
 vile-insert-below () (kbd "o")
 (interactive)
 (move-end-of-line 1)
 (newline-and-indent)
 (vile-mode 0))

(vile-define-composition
 vile-insert-above () (kbd "O")
 (interactive)
 (move-end-of-line 0)
 (newline-and-indent)
 (vile-mode 0))

(vile-define-composition
 vile-append () (kbd "a")
 (interactive)
 ;; TODO: Handle case where point is at end of line appropriately.
 (vile-forward-char)
 (vile-mode 0))

(vile-define-composition
 vile-insert-at-first-nonblank () (kbd "I")
 (interactive)
 (back-to-indentation)
 (vile-mode 0))

(vile-define-composition
 vile-insert-at-beginning-of-line () (kbd "gI")
 (interactive)
 (move-beginning-of-line 1)
 (vile-mode 0))

(vile-define-composition
 vile-insert-at-end-of-line () (kbd "A")
 (interactive)
 (move-end-of-line 1)
 (vile-mode 0))

(define-key vile-mode-map "v" 'set-mark-command)
(define-key vile-mode-map "d" 'kill-region)
(define-key vile-mode-map "gc" 'comment-dwim)
(define-key vile-mode-map "i" 'vile-mode)
(define-key vile-mode-map "x" 'delete-forward-char)

(vile-define-composition
 vile-substitute-characters () "s"
 (interactive)
 (call-interactively #'delete-forward-char)
 (vile-mode 0))
(vile-define-composition
 vile-replace-chars (c n) (kbd "r")
 (interactive "c\nP")
 (call-interactively #'delete-forward-char)
 (cl-loop for i from 1 to (or n 1)
       do
       (insert c))
 (backward-char (or n 1)))

;;;; Scrolling/Positioning with window

(define-key vile-mode-map "zz" 'recenter-top-bottom)
(vile-define-composition
 vile-to-window-top () "H"
 (interactive)
 (move-to-window-line 0))
(vile-define-composition
 vile-to-window-middle () "M"
 (interactive)
 (move-to-window-line nil))
(vile-define-composition
 vile-to-window-bottom () "L"
 (interactive)
 (move-to-window-line -1))

;;;; Clipboard

(define-key vile-mode-map "y" 'kill-ring-save)
(define-key vile-mode-map "p" 'yank)

;;;; Undo:

(define-key vile-mode-map "u" 'undo)
(define-key vile-mode-map (kbd "C-r") 'undo-redo)

;;;; Search:

(define-key vile-mode-map "/" 'isearch-forward-regexp)
(define-key vile-mode-map "*" 'isearch-forward-thing-at-point)
(define-key vile-mode-map "?" 'isearch-backward-regexp)
(define-key vile-mode-map "n" 'isearch-repeat-forward)
(define-key vile-mode-map "N" 'isearch-repeat-backward)

(defvar vile-last-character-search nil
  "The last f, t, F or T search command and character.
Used by `vile-repeat-character-search' and
`vile-repeat-character-search-reverse'.
Form: (command-character . search-character).")
(vile-define-composition
 vile-character-search-forward (c) "f"
 (interactive "c")
 (setq vile-last-character-search (cons '?f c))
 (when
     (save-excursion
       (vile-forward-char)
       (search-forward (string c) (pos-eol) t))
   (goto-char (match-beginning 0))))
(vile-define-composition
 vile-character-search-forward-to (c) "t"
 (interactive "c")
 (setq vile-last-character-search (cons '?t c))
 (when
     (save-excursion
       (vile-forward-char)
       (search-forward (string c) (pos-eol) t))
   (goto-char (match-beginning 0))
   (vile-backward-char)))
(vile-define-composition
 vile-character-search-backward (c) "F"
 (interactive "c")
 (setq vile-last-character-search (cons '?F c))
 (when
     (save-excursion
       (vile-backward-char)
       (search-backward (string c) (pos-bol) t))
   (goto-char (match-beginning 0))))
(vile-define-composition
 vile-character-search-backward-to (c) "T"
 (interactive "c")
 (setq vile-last-character-search (cons '?T c))
 (when
     (save-excursion
       (vile-backward-char)
       (search-backward (string c) (pos-bol) t))
   (goto-char (match-beginning 0))
   (vile-forward-char)))

(vile-define-composition
 vile-repeat-character-search () (kbd ";")
 (interactive)
 (when vile-last-character-search
   (let ((last (car vile-last-character-search))
         (chr (cdr vile-last-character-search)))
     (funcall
      (case last
        ((?f) #'vile-character-search-forward)
        ((?F) #'vile-character-search-backward)
        ((?t) #'vile-character-search-forward-to)
        ((?T) #'vile-character-search-backward-to))
      (cdr vile-last-character-search)))))
(vile-define-composition
 vile-repeat-character-search-opposite () (kbd ",")
 (interactive)
 (when vile-last-character-search
   (let ((last (car vile-last-character-search))
         (chr (cdr vile-last-character-search)))
     (funcall
      (case last
        ((?F) #'vile-character-search-forward)
        ((?f) #'vile-character-search-backward)
        ((?T) #'vile-character-search-forward-to)
        ((?t) #'vile-character-search-backward-to))
      (cdr vile-last-character-search))
     ;; The search functions themselves set `vile-last-character-search', so we reset it here.
     (setq vile-last-character-search (cons last chr)))))

;;;; Miscellaneous:
(define-key vile-mode-map "ga" 'what-cursor-position)
(define-key vile-mode-map "gd" 'xref-find-definitions)
(define-key vile-mode-map (kbd "C-t") 'xref-pop-marker-stack)
(define-key vile-mode-map (kbd "gO") 'imenu)

;;;; Vim "Command Mode":
(defcustom vile-commands
  (list '("w" . save-buffer)
        '("wa" . save-some-buffers)
        '("wqa" . save-buffers-kill-terminal)
        '("s" . query-replace-regexp)
        '("e" . find-file)
        '("b" . switch-to-buffer))
  "The set of command mode commands in Vile's : prompt."
  :type '(alist :value-type (string command))
  :group 'vile)

(defun vile-command-menu ()
  "Provide a menu similar to Vim's command menu.
If the input command is not matched, but starts with a ! or &,
the suffix of the string is passed to `shell-command' or
`async-shell-command' respectively."
  (interactive)
  (let* ((command-name
         (completing-read ":" (mapcar #'car vile-commands)))
         (command
          (or (cdr (assoc command-name vile-commands))
              command-name)))
    (unless (null command)
      (if (stringp command)
          (let ((first-character
                 (aref command-name 0)))
            (cl-case first-character
              ((?!) (shell-command (substring command-name 1)))
              ((?&) (async-shell-command (substring command-name 1)))
              ))
        (call-interactively command)))))
(define-key vile-mode-map (kbd ":") 'vile-command-menu)

;;;; Mode:

(define-minor-mode vile-mode
  "Minor mode for Vile keybindings."
  :global t
  :lighter " VN"                        ; Vile Normal Mode
  :group 'vile)

;; Vile mode is generally an annoyance in the minibuffer,
;; so automatically toggle it when moving in and out.
(defvar vile-was-enabled-before-minibuffer vile-mode
  "Whether vile was enabled before entering the minibuffer.")
(add-hook 'minibuffer-setup-hook
          (lambda ()
            (setq vile-was-enabled-before-minibuffer vile-mode)
            (vile-mode 0)))
(add-hook 'minibuffer-exit-hook
          (lambda ()
            (if vile-was-enabled-before-minibuffer
                (vile-mode 1))))

(provide 'vile)
;;; vile.el ends here
